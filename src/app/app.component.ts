import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>Hello World!</h1>  
    <button mat-raised-button color="primary">Button</button>
  `,
  styles: []
})
export class AppComponent { }
