# Angular Material UI
**To read**: [https://epa.ms/angular-material-ui]

**Estimated reading time**: 10 minutes

## Story Outline
This particular story explains how to setup your Angular application to begin using Material Design components. It includes information on installing, adding and displaying a Material Design components in your application to verify your setup.

## Story Organization
**Story Branch**: master
> `git checkout master`

Tags: #angular, #typescript, #html, #scss
